package com.bibao.boot.proxy;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.bibao.boot.model.TaxRate;

//@FeignClient(name="taxrate-service", url="localhost:8000")
@FeignClient(name="taxrate-service")
@RibbonClient(name="taxrate-service")
public interface TaxRateServiceProxy {
	@GetMapping("/taxrate-service/rest/filingtype/{filingType}")
	public TaxRate retrieveTaxRate(@PathVariable("filingType") String filingType);
}

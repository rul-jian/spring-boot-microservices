package com.bibao.boot.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.bibao.boot.model.FilingType;
import com.bibao.boot.model.TaxRate;
import com.bibao.boot.service.TaxRateService;

@RestController
public class TaxRateRestService {
	@Autowired
	private Environment env;
	
	@Autowired
	private TaxRateService service;
	
	@GetMapping("/filingtype/{filingType}")
	public TaxRate findTaxRate(@PathVariable("filingType") String filingType) {
		TaxRate taxRate = service.getTaxRate(FilingType.getFilingType(filingType));
		taxRate.setPort(env.getProperty("server.port"));
		return taxRate;
	}
}

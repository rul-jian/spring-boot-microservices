package com.bibao.boot.model;

public enum FilingType {
	SINGLE, MARRIED;
	
	public static FilingType getFilingType(String input) {
		for (FilingType filingType: FilingType.values()) {
			if (filingType.toString().equalsIgnoreCase(input)) {
				return filingType;
			}
		}
		return null;
	}
}

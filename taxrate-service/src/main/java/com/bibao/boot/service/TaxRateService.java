package com.bibao.boot.service;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.bibao.boot.model.FilingType;
import com.bibao.boot.model.TaxRate;

@Service
public class TaxRateService {
	private final int[] LEVEL_FOR_SINGLE = {0, 9525, 38700, 82500, 157500, 200000, 500000};
	private final int[] LEVEL_FOR_MARRIED = {0, 19050, 77400, 165000, 315000, 400000, 600000};
	private final double[] RATE_LEVEL = {0.1, 0.12, 0.22, 0.24, 0.32, 0.35, 0.37};
	private final int NUM_OF_LEVEL = 7;
	
	public TaxRate getTaxRate(FilingType filingType) {
		TaxRate taxRate = new TaxRate();
		taxRate.setFilingType(filingType);
		taxRate.setRate(new HashMap<>());
		this.determineTaxRate(taxRate);
		return taxRate;
	}
	
	private void determineTaxRate(TaxRate taxRate) {
		switch (taxRate.getFilingType()) {
		case SINGLE:
			for (int i=0; i<NUM_OF_LEVEL; i++) {
				taxRate.getRate().put(LEVEL_FOR_SINGLE[i], RATE_LEVEL[i]);
			}
			break;
		case MARRIED:
			for (int i=0; i<NUM_OF_LEVEL; i++) {
				taxRate.getRate().put(LEVEL_FOR_MARRIED[i], RATE_LEVEL[i]);
			}
			break;
		}
	}
}
